package ir.fardup.insurance.customer.service;

import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import ir.fardup.insurance.customer.persistence.Customer;
import ir.fardup.insurance.customer.persistence.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class CustomerserviceImpl implements CustomerService {

    private CustomerRepository customerRepository;


    @Autowired
    public CustomerserviceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }



        @Override
        public CustomerModel create(CustomerModel customerModel) {
        Customer customer=customerRepository.save(convertToDto(customerModel));
        customerModel.setId(customer.getId());
            return customerModel;
        }

        public static Customer convertToDto(CustomerModel customerModel)
        {
            Customer customer=new Customer();
            customer.setId(customerModel.getId());
            customer.setCname(customerModel.getCname());
            customer.setCLastname(customerModel.getCLastname());
            customer.setPhoneNumber(customerModel.getPhoneNumber());
            customer.setAddress(customerModel.getAddress());
            customer.setInsuranceType(customerModel.getInsuranceType());
            customer.setRegistrationType(customerModel.getRegistrationType());

            return  customer;
        }

    @Override
    public void delete(Integer id){
        Customer customer = getCustomer(id);
        customerRepository.delete(customer);
     }



    public Customer getCustomer(Integer id) {
        return customerRepository.findById(id).orElseThrow(() -> new RuntimeException("Unavailable"));
    }


    @Override
        public CustomerModel update(CustomerUpdateModel customerUpdateModel)
        {
           Customer customer =getCustomer(customerUpdateModel.getId());

           customer.setId(customerUpdateModel.getId());

          if(customerUpdateModel.getCLastname()!=null&&!(customerUpdateModel.getCLastname().equals(customer.getCLastname()))){
            customer.setCLastname(customerUpdateModel.getCLastname());}

           if(customerUpdateModel.getCname()!=null&&!(customerUpdateModel.getCname().equals(customer.getCname()))) {
               customer.setCname(customerUpdateModel.getCname());
           }
            if(customerUpdateModel.getAddress()!=null&&!(customerUpdateModel.getAddress().equals(customer.getAddress()))){
            customer.setAddress(customerUpdateModel.getAddress());}
            if(customerUpdateModel.getPhoneNumber()!=null&&!(customerUpdateModel.getPhoneNumber().equals(customer.getPhoneNumber()))){
                customer.setPhoneNumber(customerUpdateModel.getPhoneNumber());}
          customerRepository.save(customer);
            return convertEntityToModel(customer);
        }




        @Override
        public List<CustomerModel> index()
        {
          return customerRepository.findAll().stream().map(CustomerserviceImpl::convertEntityToModel).collect(Collectors.toList());
        }


    public static CustomerModel convertEntityToModel(Customer customer) {
        CustomerModel customerModel = new CustomerModel();
        customerModel.setId(customer.getId());
        customerModel.setCname(customer.getCname());
        customerModel.setCLastname(customer.getCLastname());
        customerModel.setPhoneNumber(customer.getPhoneNumber());
        customerModel.setAddress(customer.getAddress());

        return customerModel;

}}