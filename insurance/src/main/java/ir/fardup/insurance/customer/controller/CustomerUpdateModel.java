package ir.fardup.insurance.customer.controller;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
public class CustomerUpdateModel {

    @NotNull
    private Integer id;

    private  String cname;

    private String cLastname;

    private  String address;

    private  String phoneNumber;

}
