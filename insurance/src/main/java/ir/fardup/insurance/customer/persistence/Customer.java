package ir.fardup.insurance.customer.persistence;

import ir.fardup.insurance.configuration.Runconfiguration;
import ir.fardup.insurance.employment.persistance.Marketer;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name ="customer", schema = Runconfiguration.DB)

public class Customer {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="cname")
    private  String cname;

    @Column(name="clastname")
    private String cLastname;

   // @Column(name="insurance_number",updatable = false,columnDefinition = "CHAR(5)")
   // private  String insuranceNumber;

    //@Column(name="payment")
    //private  Integer payment;

    @Column(name="address")
    private  String address;

    @Column(name="phoneNumber")
    private  String phoneNumber;

    @Column(name="registration_type")
    @Enumerated(EnumType.STRING)
    private RegisterType registrationType;

    @Column(name="insurance_type",nullable=false)
    @Enumerated(EnumType.STRING)
    private InsuranceType insuranceType;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "marketer_id", referencedColumnName = "id")
    private Marketer marketer;
}

