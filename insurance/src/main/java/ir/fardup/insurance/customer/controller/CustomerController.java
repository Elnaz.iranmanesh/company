package ir.fardup.insurance.customer.controller;

import ir.fardup.insurance.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/customers")
public class CustomerController {


    CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }



        @GetMapping(value={"","/"})
        public List<CustomerModel> index(){
        return customerService.index();

        }

        @PostMapping(value={"","/"})
        public CustomerModel create(@RequestBody CustomerModel customerModel){
            return customerService.create(customerModel);
        }


        @PutMapping(value={"","/"})
        public CustomerModel update(@RequestBody CustomerUpdateModel customerUpdateModel) {
            return customerService.update(customerUpdateModel);

        }

        @DeleteMapping(value={"/{id}"})
        public ResponseEntity delete(@PathVariable Integer id){
             customerService.delete(id);
             return new ResponseEntity(HttpStatus.OK);
        }

}

