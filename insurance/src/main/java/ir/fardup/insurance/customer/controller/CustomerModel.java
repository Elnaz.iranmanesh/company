package ir.fardup.insurance.customer.controller;
import ir.fardup.insurance.customer.persistence.InsuranceType;
import ir.fardup.insurance.customer.persistence.RegisterType;
import lombok.Data;


@Data
public class CustomerModel {

    private  String cname;
    private Integer id;
    private String cLastname;
    private RegisterType registrationType;
    private InsuranceType insuranceType;
    private  String address;
    private  String phoneNumber;
}
