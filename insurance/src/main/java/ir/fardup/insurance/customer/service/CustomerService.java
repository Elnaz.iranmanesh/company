package ir.fardup.insurance.customer.service;

import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import ir.fardup.insurance.customer.persistence.Customer;

import java.util.List;


public interface CustomerService {

        CustomerModel create(CustomerModel customerModel);

         void delete(Integer id);

     CustomerModel  update(CustomerUpdateModel customerUpdateModel);

        List<CustomerModel> index();

}
