package ir.fardup.insurance.account.service;

import ir.fardup.insurance.account.controller.AccountModel;
import ir.fardup.insurance.account.controller.AccountUpdate;
import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;

import java.util.List;

public interface AccountService {

    AccountModel create(AccountModel accountModel);

    void delete(Integer id);

   AccountModel  update(AccountUpdate accountUpdate);

    List<AccountModel> index();

    //AccountModel accountsalary (AccountModel accountModel);
}
