package ir.fardup.insurance.account.controller;


import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.Column;

@Data
public class AccountUpdate {
@NotNull
    private  Integer id;

    private  Integer marketingSalary;

    private Integer mdSalary;
}
