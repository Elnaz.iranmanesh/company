package ir.fardup.insurance.account.persistence;

import ir.fardup.insurance.configuration.Runconfiguration;
import ir.fardup.insurance.customer.persistence.Customer;
import ir.fardup.insurance.employment.persistance.Marketer;
import ir.fardup.insurance.employment.persistance.MarketingDirector;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name ="account",schema =Runconfiguration.DB)

  public class Account {

  @Id
  @Column(name="id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;


    @Column(name="marketingSalary")
    private  Integer marketingSalary;

    @Column(name="mdSalary")
    private Integer mdSalary;

 @Column(name="marketer_id")
  private Integer marketer_id;

 @Column(name="marketer_salary_Coefficient")
  private Integer marketer_salaryCoefficient;


    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private Collection<Marketer> marketer;


    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private Collection<MarketingDirector>marketingDirectors;

}

