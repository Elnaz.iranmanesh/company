package ir.fardup.insurance.account.controller;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.Column;

@Data
public class AccountModel {

    private Integer id;
    private Integer marketingSalary;
    private Integer mdSalary;

    private Integer marketer_id;

    private Integer marketer_salaryCoefficient;

}
