package ir.fardup.insurance.account.service;


import ir.fardup.insurance.account.controller.AccountModel;
import ir.fardup.insurance.account.controller.AccountUpdate;
import ir.fardup.insurance.account.persistence.Account;
import ir.fardup.insurance.account.persistence.AccountRepository;
import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import ir.fardup.insurance.customer.persistence.Customer;
import ir.fardup.insurance.employment.controller.MarketerModel;
import ir.fardup.insurance.employment.persistance.Marketer;
import ir.fardup.insurance.employment.service.MarketerServic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {


    private  AccountRepository accountRepository;

    private MarketerServic marketerServic;


    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, MarketerServic marketerServic) {
        this.accountRepository = accountRepository;
         this.marketerServic = marketerServic;
    }


    @Override
    public AccountModel create(AccountModel accountModel) {

    Account account=accountRepository.save(convertToEntity(accountModel));
   return convertToDto(accountRepository.save(convertToEntity(accountModel)));
     // MarketerModel marketerModel=new MarketerModel();
    //  marketerServic.create(marketerModel);
        //List<MarketerModel> marketerModels=new ArrayList<>();
      //  accountModel.getMarketer_salaryCoefficient
      //  return accountModel;
    }

    public static AccountModel convertToDto(Account account)
    {
       AccountModel accountModel=new AccountModel();
        accountModel.setId(account.getId());
        accountModel.setMdSalary(account.getMdSalary());
        accountModel.setMarketingSalary(account.getMarketingSalary());
        if(account.getMarketer_id()!=null&&account.getMarketer_salaryCoefficient()!=null)
        accountModel.setMarketer_id(account.getMarketer_id());
        accountModel.setMarketer_salaryCoefficient(account.getMarketer_salaryCoefficient());
        return accountModel;
    }





    public static MarketerModel convertToDto(Marketer marketer)
    {
       MarketerModel marketerModel=new MarketerModel();
       marketerModel.setId(marketer.getId());
       marketerModel.setSalaryCoefficient(marketer.getSalaryCoefficient());

        return marketerModel;
    }


    public Account getAccount(Integer id) {
        return accountRepository.findById(id).orElseThrow(() -> new RuntimeException("Unavailable"));
    }



    @Override

    public AccountModel update(AccountUpdate accountUpdate)
    {
      Account account=getAccount(accountUpdate.getId());

      account.setId(accountUpdate.getId());

        if(accountUpdate.getMarketingSalary()!=null&&!(accountUpdate.getMarketingSalary().equals(account.getMarketingSalary()))){
            account.setMarketingSalary(accountUpdate.getMarketingSalary());}

        if(accountUpdate.getMdSalary()!=null&&!(accountUpdate.getMdSalary().equals(account.getMdSalary()))) {
            account.setMdSalary(accountUpdate.getMdSalary());
        }

        accountRepository.save(account);
        return convertToDto(account);
    }


    @Override
    public List<AccountModel> index()
    {
        return accountRepository.findAll().stream().map(AccountServiceImpl::convertToDto).collect(Collectors.toList());
    }


 /*@Override
   public AccountModel accountsalary (AccountModel accountModel)
   {

        Account account=new Account();
        if(account.getMarketer_salaryCoefficient


       //  return accountModel;


       return accountModel;
   }


*/






    public static Account convertToEntity (AccountModel accountModel) {
        Account account = new Account();

        account.setId(accountModel.getId());
        account.setMarketingSalary(accountModel.getMarketingSalary());
        account.setMdSalary(accountModel.getMdSalary());

        if(accountModel.getMarketer_id()!=null&&accountModel.getMarketer_salaryCoefficient()!=null)
            account.setMarketer_id(accountModel.getMarketer_id());
        account.setMarketer_salaryCoefficient(accountModel.getMarketer_salaryCoefficient());

        return account;

    }

    public static Marketer convertToEntity(MarketerModel marketerModel)
    {
        Marketer marketer=new Marketer();
        marketer.setId(marketerModel.getId());
        marketer.setSalaryCoefficient(marketerModel.getSalaryCoefficient());

        return marketer;
    }


    @Override
    public void delete(Integer id){
        Account account = getAccount(id);
        accountRepository.delete(account);
    }

}
