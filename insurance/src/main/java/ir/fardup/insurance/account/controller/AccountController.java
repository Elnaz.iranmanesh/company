package ir.fardup.insurance.account.controller;


import ir.fardup.insurance.account.service.AccountService;
import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountController {


    AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @DeleteMapping(value={"/{id}"})
    public ResponseEntity delete(@PathVariable Integer id){
        accountService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

        @GetMapping(value={"","/"})
        public List<AccountModel> index(){
            return accountService.index();

        }

    @PostMapping(value ={"", "/"})
    public AccountModel create(@RequestBody AccountModel accountModel)
    {
        return accountService.create(accountModel);
    }


    @PutMapping(value={"","/"})
    public AccountModel update(@RequestBody AccountUpdate accountUpdate) {
        return accountService.update(accountUpdate);

    }

           // AccountModel accountsalary (AccountModel accountModel)

}









