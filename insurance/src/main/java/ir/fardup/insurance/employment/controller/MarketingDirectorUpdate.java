package ir.fardup.insurance.employment.controller;


import lombok.Data;

@Data
public class MarketingDirectorUpdate {

    private Integer id;

    private  String dName;

    private  String address;

    private  String dLastname;
    private Integer salaryCoefficient;
    private  String phoneNumber;
}
