package ir.fardup.insurance.employment.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketerModel {

    private Integer id;

    private String mName;

    private String mLastname;

    private String phoneNumber;

    private Integer parentId;

    private String address;

    private Integer salaryCoefficient;

    private Integer salesAmount;

    private Integer account_id;

    //private  Integer account_marketingSalary;
}
