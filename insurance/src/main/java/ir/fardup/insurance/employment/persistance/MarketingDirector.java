package ir.fardup.insurance.employment.persistance;

import ir.fardup.insurance.account.persistence.Account;
import ir.fardup.insurance.configuration.Runconfiguration;
import ir.fardup.insurance.customer.persistence.RegisterType;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;


@Entity
@Data
@Table(name = "marketingdirector", schema = Runconfiguration.DB)
public class MarketingDirector {



    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="dname")
    private  String dName;

    @Column(name="addresse")
    private  String address;

    @Column(name="dlastname")
    private  String dLastname;

    @Column(name="phonenumber")
    private  String phoneNumber;


    @Column(name="salary_coefficient")
    private Integer salaryCoefficient;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;


    @OneToMany(mappedBy="marketingdirector", fetch = FetchType.LAZY)
    Collection<Marketer> marketerlist;

}
