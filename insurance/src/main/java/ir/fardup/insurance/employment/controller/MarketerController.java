package ir.fardup.insurance.employment.controller;


import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import ir.fardup.insurance.employment.service.MarketerServic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/marketer")
public class MarketerController {

    MarketerServic marketerServic;
    @Autowired
    public MarketerController(MarketerServic marketerServic) {
        this.marketerServic = marketerServic;
    }


    @GetMapping(value={"","/"})
    public List<MarketerModel> index(){
        return marketerServic.index();


    }

    @PostMapping(value={"","/"})
    public MarketerModel create(@RequestBody MarketerModel marketerModel){
        return marketerServic.create(marketerModel);
    }


    @PutMapping(value={"","/"})
    public MarketerModel update(@RequestBody MarketerUpdateModel marketerUpdateModel) {
        return marketerServic.update(marketerUpdateModel);

    }

    @DeleteMapping(value={"/{id}"})
    public ResponseEntity delete(@PathVariable Integer id){
        marketerServic.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
