package ir.fardup.insurance.employment.service;

import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import ir.fardup.insurance.employment.controller.MarketerModel;
import ir.fardup.insurance.employment.controller.MarketingDirectorModel;
import ir.fardup.insurance.employment.controller.MarketingDirectorUpdate;
import ir.fardup.insurance.employment.persistance.MarketingDirector;

import java.util.List;

public interface MarketingDirectorService {

    MarketingDirectorModel create(MarketingDirectorModel marketingdirectorModel);


    void delete(Integer id);

    MarketingDirectorModel update(MarketingDirectorUpdate marketingDirectorUpdate);

    List<MarketingDirectorModel> index();
}
