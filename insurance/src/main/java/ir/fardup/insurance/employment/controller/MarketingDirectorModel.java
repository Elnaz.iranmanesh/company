package ir.fardup.insurance.employment.controller;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class MarketingDirectorModel {

    private Integer salaryCoefficient;
    private Integer id;

    private  String dName;

    private  String address;

    private  String dLastname;

    private  String phoneNumber;

}
