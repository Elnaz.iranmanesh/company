package ir.fardup.insurance.employment.service;


import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import ir.fardup.insurance.employment.controller.MarketerModel;

import ir.fardup.insurance.employment.controller.MarketerUpdateModel;
import ir.fardup.insurance.employment.persistance.Marketer;

import java.util.List;

public interface MarketerServic {

    MarketerModel create(MarketerModel marketerModel);

     //List<MarketerModel>create(List<MarketerModel> marketermodel);

   void delete(Integer id);

    MarketerModel update(MarketerUpdateModel marketerUpdateModel);

    List<MarketerModel> index();
}
