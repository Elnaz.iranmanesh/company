package ir.fardup.insurance.employment.service;

import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import ir.fardup.insurance.customer.persistence.Customer;
import ir.fardup.insurance.employment.controller.MarketingDirectorModel;
import ir.fardup.insurance.employment.controller.MarketingDirectorUpdate;
import ir.fardup.insurance.employment.persistance.MarketingDirector;
import ir.fardup.insurance.employment.persistance.MarketingDirectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class MarketingDirectorserviceImp implements MarketingDirectorService {


    private MarketingDirectorRepository marketingDirectorRepository;
    @Autowired
    public MarketingDirectorserviceImp(MarketingDirectorRepository marketingDirectorRepository) {
        this.marketingDirectorRepository = marketingDirectorRepository;
    }

    @Override
    public MarketingDirectorModel create(MarketingDirectorModel marketingDirectorModel) {
       MarketingDirector marketingDirector=marketingDirectorRepository.save(convertToDto(marketingDirectorModel));
      marketingDirectorModel.setId(marketingDirector.getId());
        return marketingDirectorModel;
    }

    public static MarketingDirector convertToDto(MarketingDirectorModel  marketingDirectorModel)
    {
        MarketingDirector marketingDirector=new MarketingDirector();
        marketingDirector.setId(marketingDirectorModel.getId());
        marketingDirector.setSalaryCoefficient(marketingDirectorModel.getSalaryCoefficient());
        marketingDirector.setDName(marketingDirectorModel.getDName());
        marketingDirector.setDLastname(marketingDirectorModel.getDLastname());
        marketingDirector.setAddress(marketingDirectorModel.getAddress());
        marketingDirector.setPhoneNumber(marketingDirectorModel.getPhoneNumber());
          return marketingDirector;

    }

    @Override
    public void delete(Integer id){
       MarketingDirector marketingDirector = getMarketingdirector(id);
       marketingDirectorRepository.delete(marketingDirector);
    }



    public MarketingDirector getMarketingdirector(Integer id) {
        return marketingDirectorRepository.findById(id).orElseThrow(() -> new RuntimeException("Unavailable"));
    }


    @Override
    public MarketingDirectorModel update(MarketingDirectorUpdate marketingDirectorUpdate)
    {
        MarketingDirector marketingDirector =getMarketingdirector(marketingDirectorUpdate.getId());
       //marketingDirector.setId(marketingDirectorUpdate.getId());

        if(marketingDirectorUpdate.getDName()!=null&&!(marketingDirectorUpdate.getDName().equals(marketingDirector.getDName()))){
        marketingDirector.setDName(marketingDirectorUpdate.getDName());}

        if(marketingDirectorUpdate.getDLastname()!=null&&!(marketingDirectorUpdate.getDLastname().equals(marketingDirector.getDLastname()))){
        marketingDirector.setDLastname(marketingDirectorUpdate.getDLastname());}

        if(marketingDirectorUpdate.getAddress()!=null&&!(marketingDirectorUpdate.getAddress().equals(marketingDirector.getAddress()))){
        marketingDirector.setAddress(marketingDirectorUpdate.getAddress());}

        if(marketingDirectorUpdate.getPhoneNumber()!=null&&!(marketingDirectorUpdate.getPhoneNumber().equals(marketingDirector.getPhoneNumber()))){
        marketingDirector.setPhoneNumber(marketingDirectorUpdate.getPhoneNumber());}
        marketingDirectorRepository.save(marketingDirector);
        return convertEntityToModel(marketingDirector);
    }





    @Override

    public List<MarketingDirectorModel> index()
    {
        return marketingDirectorRepository.findAll().stream().map(MarketingDirectorserviceImp::convertEntityToModel).collect(Collectors.toList());
    }

    public static MarketingDirectorModel convertEntityToModel(MarketingDirector marketingDirector) {


        MarketingDirectorModel MarketingDirectorModel = new MarketingDirectorModel();
      MarketingDirectorModel.setId(marketingDirector.getId());
        MarketingDirectorModel.setDName(marketingDirector.getDName());
        MarketingDirectorModel.setDLastname(marketingDirector.getDLastname());
        MarketingDirectorModel.setPhoneNumber(marketingDirector.getPhoneNumber());
        MarketingDirectorModel.setAddress(marketingDirector.getAddress());
        MarketingDirectorModel.setSalaryCoefficient(marketingDirector.getSalaryCoefficient());

        return MarketingDirectorModel;

    }
}
