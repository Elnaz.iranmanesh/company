package ir.fardup.insurance.employment.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface MarketerRepository extends JpaRepository<Marketer,Integer> {


}
