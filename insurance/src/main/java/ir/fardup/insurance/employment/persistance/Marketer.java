package ir.fardup.insurance.employment.persistance;

import ir.fardup.insurance.account.persistence.Account;
import ir.fardup.insurance.configuration.Runconfiguration;
import ir.fardup.insurance.customer.persistence.Customer;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;


@Entity
@Data
@Table(name = "marketer", schema = Runconfiguration.DB)
public class Marketer {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="mname")
    private String mName;

    @Column(name="mlastname")
    private String mLastname;

    @Column(name="phonenumber")
    private String phoneNumber;

    @Column(name="parent_id")
    private Integer parentId;

    @Column(name="address")
    private String address;

    @Column(name="salary_coefficient")
    private Integer salaryCoefficient;

    @Column(name="sales_amount")
    private Integer salesAmount;

    @Column(name = "account_id",insertable = false,updatable = false)
    private  Integer account_id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "marketingdirector_id", referencedColumnName = "id")
    private MarketingDirector marketingdirector;

   // @Column(name="account_marketingsalary")
   // private  Integer account_marketingSalary;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;


    @OneToMany(mappedBy = "marketer", fetch = FetchType.LAZY)
    Collection<Customer> customerlist;

}
