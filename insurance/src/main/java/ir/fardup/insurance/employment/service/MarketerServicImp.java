package ir.fardup.insurance.employment.service;

import ir.fardup.insurance.account.persistence.Account;
import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import ir.fardup.insurance.customer.persistence.Customer;
import ir.fardup.insurance.employment.controller.MarketerModel;
import ir.fardup.insurance.employment.controller.MarketerUpdateModel;
import ir.fardup.insurance.employment.persistance.Marketer;
import ir.fardup.insurance.employment.persistance.MarketerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MarketerServicImp implements MarketerServic {

    private MarketerRepository marketerRepository;


    @Autowired

    public MarketerServicImp(MarketerRepository marketerRepository) {
        this.marketerRepository = marketerRepository;
    }

    @Override
   public MarketerModel create(MarketerModel marketerModel) {

   // public  List<MarketerModel>create(List<MarketerModel> marketermodel){
       return convertToDto(marketerRepository.save(convertToEntity(marketerModel)));

        //marketerRepository.saveAll(marketermodel.stream().map(MarketerServicImp::convertToEntity).collect(Collectors.toList()));

    }

    public static Marketer convertToEntity(MarketerModel marketerModel)
    {
        Marketer marketer=new Marketer();
        marketer.setId(marketerModel.getId());
        marketer.setMName(marketerModel.getMName());
        marketer.setMLastname(marketerModel.getMLastname());
        marketer.setPhoneNumber(marketerModel.getPhoneNumber());
        marketer.setAddress(marketerModel.getAddress());
        marketer.setParentId(marketerModel.getParentId());
        marketer.setSalaryCoefficient(marketerModel.getSalaryCoefficient());
       Account account=new Account();
        account.setId(marketerModel.getAccount_id());
        marketer.setAccount(account);
        return  marketer;
    }

    @Override
    public void delete(Integer id){
        Marketer marketer = getMarketer(id);
        marketerRepository.delete(marketer);
    }



    public Marketer getMarketer(Integer id) {
        return marketerRepository.findById(id).orElseThrow(() -> new RuntimeException("Unavailable"));
    }



    @Override
    public MarketerModel update(MarketerUpdateModel marketerUpdateModel)
    {
       Marketer marketer =getMarketer(marketerUpdateModel.getId());

       marketer.setId(marketerUpdateModel.getId());
        if(marketerUpdateModel.getMLastname()!=null&&!(marketerUpdateModel.getMLastname().equals(marketer.getMLastname()))){
        marketer.setMLastname(marketerUpdateModel.getMLastname());}
        if(marketerUpdateModel.getMName()!=null&&!(marketerUpdateModel.getMName().equals(marketer.getMName()))){
        marketer.setMName(marketerUpdateModel.getMName());}

        if(marketerUpdateModel.getAddress()!=null&&!(marketerUpdateModel.getAddress().equals(marketer.getAddress()))){
        marketer.setAddress(marketerUpdateModel.getAddress());}

        if(marketerUpdateModel.getSalesAmount()!=null&&!(marketerUpdateModel.getSalesAmount().equals(marketer.getSalesAmount()))){
            marketer.setSalesAmount(marketerUpdateModel.getSalesAmount());}

        if(marketerUpdateModel.getPhoneNumber()!=null&&!(marketerUpdateModel.getPhoneNumber().equals(marketer.getPhoneNumber()))){
        marketer.setPhoneNumber(marketerUpdateModel.getPhoneNumber());}

        marketerRepository.save(marketer);
        return convertToDto(marketer);
    }



    @Override
    public List<MarketerModel> index()
    {
        return marketerRepository.findAll().stream().map(MarketerServicImp::convertToDto).collect(Collectors.toList());
    }


    public static MarketerModel convertToDto(Marketer marketer) {
      MarketerModel marketerModel = new MarketerModel();
        marketerModel.setId(marketer.getId());
        marketerModel.setMName(marketer.getMName());
        marketerModel.setMLastname(marketer.getMLastname());
        marketerModel.setPhoneNumber(marketer.getPhoneNumber());
        marketerModel.setAddress(marketer.getAddress());
        marketerModel.setParentId(marketer.getParentId());
        marketerModel.setSalesAmount(marketer.getSalesAmount());
        //marketerModel.setAccount_marketingSalary(marketer.getAccount_marketingSalary());
        Optional.ofNullable(marketer.getAccount())
                .ifPresent(value -> marketerModel.setAccount_id(value.getId()));
        return marketerModel;

    }





    }
