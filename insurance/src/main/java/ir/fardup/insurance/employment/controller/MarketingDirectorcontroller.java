package ir.fardup.insurance.employment.controller;

import ir.fardup.insurance.customer.controller.CustomerModel;
import ir.fardup.insurance.customer.controller.CustomerUpdateModel;
import ir.fardup.insurance.employment.service.MarketingDirectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("marketingdirector/")
public class MarketingDirectorcontroller {


    MarketingDirectorService marketingdirectorService;
    @Autowired
    public MarketingDirectorcontroller(MarketingDirectorService marketingdirectorService) { this.marketingdirectorService = marketingdirectorService;
    }



    @GetMapping(value={"","/"})
    public List<MarketingDirectorModel> index(){
        return marketingdirectorService.index();}


    @PostMapping(value={"","/"})
    public MarketingDirectorModel create(@RequestBody MarketingDirectorModel marketingdirectorModel) {
        return marketingdirectorService.create(marketingdirectorModel);
    }

    @PutMapping(value={"","/"})
    public MarketingDirectorModel update(@RequestBody MarketingDirectorUpdate marketingDirectorUpdate) {
        return marketingdirectorService.update(marketingDirectorUpdate);

    }

    @DeleteMapping(value={"/{id}"})
    public ResponseEntity delete(@PathVariable Integer id){
        marketingdirectorService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
