package ir.fardup.insurance.employment.controller;


import lombok.Data;

@Data
public class MarketerUpdateModel {

    private Integer id;

    private String mName;

    private String mLastname;

    private String phoneNumber;

    private String address;

    private Integer salesAmount;
}
